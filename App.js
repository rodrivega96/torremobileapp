/**
 * Torre React Native Mobile Application
 *  by Rodrigo Vega Gimenez
 *  GitLab: https://gitlab.com/rodrivega96/
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect, Fragment } from 'react';
import type {Node} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';  
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  TouchableHighlight
} from 'react-native';
import { Picker } from '@react-native-community/picker';
import moment from "moment";
import Spinner from 'react-native-loading-spinner-overlay';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faExclamation, faInfoCircle, faCalendarAlt } from '@fortawesome/free-solid-svg-icons';
import { useNavigation } from '@react-navigation/native';
import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const Section = ({children, title}): Node => {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};

const Principal: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const navigation = useNavigation()
  const [loading, setLoading] = useState(false)

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <View style={styles.headerContainer}>
        <View style={styles.logoContainer}>
          <Image style={styles.logo} source={require('../torreMobile/src/assets/torre_name.png')} />
          <Text style={styles.titleHighlight}>Welcome to Torre Mobile App!</Text>
        </View>
      </View>
      <View style={styles.separator}></View>
        <View
          style={{
            backgroundColor: isDarkMode ? Colors.black : Colors.white,
          }}>
          <Section title="About the app...">
            Hi! My name is Rodrigo Vega Gimenez, and this is my technical test to show my knowledge for the position of Software Developer Intern for Torre.
            The main reason why I decide to make a mobile application is my fresh practice developing the mobile app for my final engineering project.
            Any inconvenience, I'm always available by my personal email account <Text style={styles.numberText}>rvega389@gmail.com</Text> and my personal phone number <Text
            style={styles.numberText}>5493512471357</Text>.
          </Section>
          <Section title="People">
            The first section of this app will be intended for the consultation of people.
            Initially the system will allow you to search for all the people who meet the filters entered, such as their skill or role and the language they speak, which are displayed paginated and displayed on cards. These cards can be consulted promptly.{'\n'}{'\n'}
            <View style={styles.buttonContainer}>
              <TouchableHighlight underlayColor="white" style={styles.button} disabled={loading} onPress={ () =>  {console.log("Here is for people."),
            navigation.navigate("SearchPeople")}}> 
                <Text>Search people</Text>
              </TouchableHighlight>
            </View>   
          </Section>
          <Section title="Job Opportunities">
            The second section of this app will be intended for the consultation of job opportunities.
            As well as with people, at first you can consult for all the available job opportunities ordered by pagination and shown on cards, which can be consulted individually.{'\n'}{'\n'}
            <View style={styles.buttonContainer}>
              <TouchableHighlight underlayColor="white" style={styles.button} disabled={loading} onPress={ () =>  {console.log("Here is for job opportunities."),
            navigation.navigate("SearchJobs")}  }> 
                <Text>Search job opportunities</Text>
              </TouchableHighlight>
            </View>
            {'\n'}
          </Section>
        </View>

        <Spinner
          visible={loading}
          color='orange'
          // textContent={'Loading...'}
          // textStyle={styles.spinnerTextStyle}
        />
      </ScrollView>
    </SafeAreaView>
    
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
    color: 'white'
  },
  numberText:{
    color:'lightgreen'
  },
  titleHighlight: {
    fontWeight: '700',
    color: 'white',
    marginLeft:wp('3%'),
    fontSize: hp('2.4%')
  },
  separator:{
    borderBottomColor: 'white',
    borderBottomWidth: 1,
    marginLeft:wp('5%'),
    marginRight:wp('5%'),
    marginBottom: hp('1%'), 
  },
  headerContainer:{
    width: wp('100%'),
    height: hp('6%'),
    backgroundColor:'#36393e', 
    alignItems: 'center',
    shadowColor: 'black',
    flex: 0,
    flexDirection: 'row',
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.41,
    shadowRadius: 9.11,
    elevation: 14, 
      
  },
  logoContainer: {
    width: wp('95%'),
    marginLeft: wp('5%'),
    height: hp('8%'),
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  button: {      
    width: wp('90%'),
    height: hp('4%'),
    borderRadius:4,
    backgroundColor: "white",
    borderWidth:wp('0.4%'),
    borderColor:'lightgreen',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonContainer:{
    marginLeft: wp('60%'),
    marginTop: hp('1.5%'),
    //backgroundColor: 'red'
    },
});

export default Principal;

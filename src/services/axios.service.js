import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage';
import RNFetchBlob from'rn-fetch-blob'

export const axiosService = {
    get,
    post
}
  
function get(url) {
    return axios.get(url)
        .then(res =>{
            if(res.data && res.status===200)
                return Promise.resolve({data: res.data, statusCode: res.status})
            else return Promise.reject(res)
        })
        .catch(err => {
            return Promise.reject(err)
        })
}

async function post(url, body){    
    return axios.post(url, body,
        { headers: {    
            'Accept' : 'application/json',            
            'Content-Type': 'application/json'},
        }
        ).then(res =>{             
        if(res.data && (res.status===201 || res.status === 200))
            return Promise.resolve({data: res.data, statusCode: res.status})
        else return Promise.reject(res)
    })
    .catch(err => {              
        return Promise.reject(err)
    })
}

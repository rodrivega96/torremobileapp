import axios from 'axios'
import {axiosService} from '../axios.service'

const postPeopleSearch = (size, body) => {
    return axiosService.post(`https://search.torre.co/people/_search/?offset=0&size=${size}&aggregate=true`, body)
}

const postOpportunitySearch = (size, body) => {
    return axiosService.post(`https://search.torre.co/opportunities/_search?size=${size}&aggregate=true&offset=0`, body)
}

const getPeople = (username) => {
    return axiosService.get('https://torre.bio/api/bios/' + username)
}

const getOpportunity = (jobId) => {
    return axiosService.get('https://torre.co/api/opportunities/' + jobId)
}

export const apiTorre = {
    postPeopleSearch,
    postOpportunitySearch,
    getPeople,
    getOpportunity
}
import React, {useEffect, useState} from 'react'
import { useIsFocused, navigation } from '@react-navigation/native'
import { View, SafeAreaView, StyleSheet, TouchableOpacity, Text, TextInput, Input} from 'react-native'
import { useForm, Controller } from "react-hook-form";
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { FlatList, ScrollView } from 'react-native-gesture-handler'
//import { CardPeople } from '../../ui/card-view'
import { _styles } from './styles'
import { apiTorre } from '../../services/API-TORRE/index'
import { Picker } from '@react-native-community/picker';

const styles = StyleSheet.create(_styles)

const SearchPeople = () => {
    const [size, setSize] = useState("-- Seleccione cantidad de personas --")
    const [keyword, setKeyword] = useState("-- Ingresa una habilidad o rol --")
    const [language, setLanguage] = useState("-- Seleccione un idioma --")
    const [people, setPeople] = useState()

    useEffect(() => (
        console.log(size, language, keyword)
    ), [size, language, keyword])

    const getPeople = (size) => {
        apiTorre.postPeopleSearch(size, )
    }

    return(
        <SafeAreaView style={styles.container}>
        <View style={styles.headerContainer}>  
            <View style={styles.titleHeaderContainer}>
                <Text style={styles.titleHeader}>Search people</Text>   
            </View>
        </View>
        <View style={styles.sizePicker}>
            <Text style={styles.textStyle}>Selector de tamaño (máximo de 10)</Text> 
            <View style={styles.pickerInputContainer}>
                <Picker style={styles.pickerStyle}
                    selectedValue={size}
                    onValueChange={(itemValue) => setSize(itemValue)}>
                        <Picker.Item label={size == "0" ? size:"-- Seleccione cantidad de personas --"} value="0" color='gray'/>
                        <Picker.Item label="1" value="1" />
                        <Picker.Item label="2" value="2" />
                        <Picker.Item label="3" value="3" />
                        <Picker.Item label="4" value="4" />
                        <Picker.Item label="5" value="5" />
                        <Picker.Item label="6" value="6" />
                        <Picker.Item label="7" value="7" />
                        <Picker.Item label="8" value="8" />
                        <Picker.Item label="9" value="9" />
                        <Picker.Item label="10" value="10" />
                </Picker>
            </View> 
        </View>
        <View style={styles.sizePicker}>
            <Text style={styles.textStyle}>Idiomas conocidos</Text> 
            <View style={styles.pickerInputContainer}>
                <Picker style={styles.pickerStyle}
                    selectedValue={language}
                    onValueChange={(itemValue) => setLanguage(itemValue)}>
                        <Picker.Item label={language == "" ? language:"-- Seleccione un idioma --"} value="" color='gray'/>
                        <Picker.Item label="Español" value="Español" />
                        <Picker.Item label="Inglés" value="Inglés" />
                        <Picker.Item label="Portugués" value="Portugués" />
                        <Picker.Item label="Francés" value="Francés" />
                        <Picker.Item label="Alemán" value="Alemán" />
                        <Picker.Item label="Ruso" value="Ruso" />
                </Picker>
            </View> 
        </View>
        <View style={styles.sizePicker}>
            <Text style={styles.textStyle}>Habilidad/rol</Text> 
            <View style={styles.pickerInputContainer}>
                <Input style={styles.inputs}
                placeholder={ keyword !== "" ? keyword : "-- Ingrese una habilidad o rol --"}                
                name="keyword"
                onChangeText={(e) => setKeyword(e)}
                />
            </View> 
        </View>
            <View style={styles.infoContainer}>
            {/* <ScrollView> */}

            {/* </ScrollView> */}
            </View>
    </SafeAreaView>
    )
}
export default SearchPeople;
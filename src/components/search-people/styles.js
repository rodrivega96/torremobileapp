import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'; 
export const _styles = {
    container: {
      flex: 1,
      flexDirection:'column',
      backgroundColor: '#ffffff',   
    },
    headerContainer:{
      width: wp('100%'),
      height: hp('8%'),
      backgroundColor:'#cddc39', 
      alignItems: 'center',
      shadowColor: 'black',
      flex: 0,
      flexDirection: 'row',
      shadowOffset: {
        width: 0,
        height: 7,
      },
      shadowOpacity: 0.41,
      shadowRadius: 9.11,
      
      elevation: 14, 
        
    },
    titleHeaderContainer:{
      width: wp('70%'),
      marginLeft: wp('15%'),
      height: hp('8%'),
      alignItems: 'center',
      justifyContent: 'center'
    },
    titleHeader:{
      fontSize: hp('3%'),
      color: 'white',
      fontWeight: '100'
    },
    buttonContainer:{
      width: wp('10%'),
      height: hp('8%'),
      alignItems: 'center',
      justifyContent: 'center',
      marginLeft: wp('2%'),
    },
    button:{
      color: 'white',     
    },
    dropButton:{
      color: 'white',
      marginBottom: hp('1.2%')     
    },
    separator:{
      borderBottomColor: '#F8AF3C',
      borderBottomWidth: 1,
      marginLeft:wp('5%'),
      marginRight:wp('5%')      
    },
    infoContainer: {
        width: wp('100%'),        
        height: hp('92%'),
        flex: 1
    },
    search: {
      marginTop: hp("2%"),
      marginBottom: hp("1%")
    },
    messageNotPostulationsContainer:{
      marginTop: hp('1.5%'),        
      width: wp('95%'), 
      alignItems: 'center'       
    },
    messageNotPostulations:{
      fontSize: hp('2.5%'),
      textAlign: 'center'
    },
    smallWhiteText:{
      fontSize: hp('1.5%'),
      fontWeight: 'bold',
      color: 'white',
      marginTop: hp('-0.5%')
    },
    sizePicker:{
      width: wp('90%'),
      marginLeft: wp('5%'),
      height: hp('7%'),
      marginTop: hp('1%'),
      //backgroundColor: 'red'
    },
    textStyle:{  
      margin: 0.3,  
      fontSize: hp('2%'),  
      fontWeight: 'bold',  
      textAlign: 'center',  
  },  
  pickerStyle:{  
      height: hp('4%'),  
      width: wp("95%"),  
      color: '#344953',  
      justifyContent: 'center',
      borderBottomColor: '#cddc39',
      borderBottomWidth: hp('0.5%')
  },
  pickerInputContainer: {
    marginBottom: hp('1%'),
    height: hp('4%'),
    borderColor: '#F8AF3C',
    backgroundColor: '#FFFFFF',
    borderRadius:4,
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems:'center',
    fontSize: hp('3%'),
    width: wp('90%')
  },
  inputs:{        
    marginLeft:wp('1%'),
    borderBottomColor: '#FFFFFF',
    fontSize: hp('2%'),
    color: 'black',
    width:wp('90%'),
    minHeight: hp('7%'),
  },
}
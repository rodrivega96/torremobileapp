/**
 * @format
 */
import 'react-native-gesture-handler';
import React, { useState, useEffect, Fragment } from 'react';
import {AppRegistry} from 'react-native';
import Principal from './App';
import {name as appName} from './app.json';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionSpecs, HeaderStyleInterpolators } from '@react-navigation/stack';
import { Provider as PaperProvider} from 'react-native-paper';

import SearchPeople from './src/components/search-people/index';
import SearchJobs from './src/components/search-opportunity/index';
import GetJob from './src/components/puntual-opportunity/index';
import GetUser from './src/components/punctual-user/index';

const Stack = createStackNavigator();
const transitions = {
        gestureDirection: 'horizontal',
        transitionSpec: {
          open: TransitionSpecs.FadeInFromBottomAndroidSpec,
          close: TransitionSpecs.FadeInFromBottomAndroidSpec,
        },
        headerStyleInterpolator: HeaderStyleInterpolators.forFade,
        cardStyleInterpolator: ({ current, layouts }) => {
          return {
            cardStyle: {
              transform: [
                {
                  translateX: current.progress.interpolate({
                    inputRange: [0, 1],
                    outputRange: [layouts.screen.width, 0],
                  }),
                }
              ],
            },
            overlayStyle: {
              opacity: current.progress.interpolate({
                inputRange: [0, 1],
                outputRange: [0, 0.5],
              }),
            },
          };
        },
    }

const App = () => {
    return(
        <PaperProvider>
                <NavigationContainer>
                    <Stack.Navigator initialRouteName={"Principal"}>
                    <Stack.Screen 
                            name="Principal"
                            component={Principal}
                            options= {{
                                headerShown: false,
                                ...transitions                         
                            }}
                            
                            
                        />
                        <Stack.Screen 
                            name="SearchPeople"
                            component={SearchPeople}
                            options= {{
                                headerShown: false,
                                ...transitions                         
                            }}              
                        />
                        <Stack.Screen 
                            name="SearchJobs"
                            component={SearchJobs}
                            options= {{
                                headerShown: false,
                                ...transitions                         
                            }}    
                        />
                        <Stack.Screen 
                            name="GetJob"
                            component={GetJob}
                            options= {{
                                headerShown: false,
                                ...transitions                         
                            }}        
                        />
                        <Stack.Screen 
                            name="GetUser"
                            component={GetUser}
                            options= {{
                                headerShown: false,
                                ...transitions                         
                            }}       
                        />       
                    </Stack.Navigator>
                </NavigationContainer> 
            </PaperProvider>             
    )
}

AppRegistry.registerComponent(appName, () => App);
